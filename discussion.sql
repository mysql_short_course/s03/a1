-- [SECTION] Inserting Records/Row

-- Syntax: INSERT INTO <table_name>(columns) VALUES (values);

INSERT INTO artists(name) VALUES ("Rivermaya"); 
INSERT INTO artists(name) VALUES ("Blackpink");
INSERT INTO artists(name) VALUES ("Taylor Swift");
INSERT INTO artists(name) VALUES ("New Jeans");
INSERT INTO artists(name) VALUES ("Bamboo");

INSERT INTO albums(album_title, date_released, artists_id) VALUES ("Trip", "1996-01-01", 1);

INSERT INTO albums(album_title, date_released, artists_id) VALUES ("Born Pink", "2002-09-16", 2);
INSERT INTO albums(album_title, date_released, artists_id) VALUES ("The Album", "2020-10-02", 2);

INSERT INTO albums(album_title, date_released, artists_id) VALUES ("Midnights", "2022-10-21", 3);

INSERT INTO albums(album_title, date_released, artists_id) VALUES ("New Jeans", "2022-08-01", 4);


-- Add new album released by Bamboo or other artist/band;

INSERT INTO albums(album_title, date_released, artists_id) VALUES ("Light Peace Love", "2005-08-01", 5);

INSERT INTO albums(album_title, date_released, artists_id) VALUES ("As the Music Plays", "2002-08-01", 5);

INSERT INTO albums(album_title, date_released, artists_id) VALUES ("Fearless", "2008-11-11", 3), ("Kill This Love", "2019-01-01", 2);

-- insert songs
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Snow on the Beach", 256, "Pop", 4), ("Anti-Hero", 201, "Pop", 4);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Bejeweled", 314, "Pop", 4);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Masaya", 450, "Jazz", 8), ("Mr. Clay", 357, "Jazz", 8), ("Noypi", 700, "OPM", 8);

-- Mini activity
INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Kundiman", 528, "Pop", 1), ("Monopoly", 444, "Rock", 1);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Shutdown", 256, "Hip-Pop", 2), ("Pink Venom", 307, "Hip-hop", 2);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Ice Cream", 256, "K-Pop", 3), ("How You Like That", 301, "K-Pop", 3);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Attention", 422, "K-Pop", 5), ("OMG", 340, "K-Pop", 5);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Children of The Sun", 333, "Alternative rock", 6), ("Truth", 420, "Alternative rock", 6);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("You Belong Wtih Me", 351, "Country pop", 7), ("Breathe", 424, "Country pop", 7);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("You Belong Wtih Me", 351, "Country pop", 7), ("Breathe", 424, "Country pop", 7);

INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Ddu-Du Ddu-Du", 336, "Pop", 8), ("Kick It", 359, "Pop", 8);

-- [Section] Read data from our database
-- Syntax
    -- SELECT <column_name> FROM <table_name>;

SELECT * FROM songs;

-- Specify columns that will be shown
SELECT song_name FROM songs;

SELECT song_name, genre FROM songs;

-- To filter the output of the SELECT operation
SELECT * FROM songs WHERE genre = "OPM";

SELECT * FROM albums WHERE artists_id = 3;

--We can use AND and OR ketword for multiple expressions in the WHERE clause
-- Display the title and length of the OPEM songs that are more than 4 mins

SELECT song_name, length FROM songs WHERE length > 400 AND genre = "Pop";

SELECT song_name, genre FROM songs WHERE genre = "Pop" OR genre = "Jazz";

-- [Section] UPDATING RECORDS/DATA
-- UPDATE <table_name> SET <column_name> = <value_tobe> WHERE <condiition>

UPDATE songs SET length = 428 WHERE song_name = "Kundiman";

UPDATE songs SET genre = "Original Pinoy Music" WHERE genre = "OPM";

-- [Section] DELETING RECORDS
-- DELETE FROM <table_name> WHERE <condition>;
-- Delete all OPM songs that are more than 4 minutes

DELETE FROM songs WHERE genre = "Original Pinoy Music" AND length > 400;

DELETE FROM songs WHERE genre = "Pop" AND length > 500;