INSERT INTO users (email, password, datetime_created) 
    VALUES  ("johnsmith@gmail.com", "passwordA", "2021-01-01"),
            ("juandelacruz@gmail.com", "passwordB", "2021-01-01"),
            ("janesmith@gmail.com", "passwordC", "2021-01-01"),
            ("mariadelacruz@gmail.com", "passwordD", "2021-01-01"),
            ("johndoe@gmail.com", "passwordE", "2021-01-01");

INSERT INTO posts (user_id, title, content, datetime_posted)
    VALUES  (3, "First Code", "Hello World!", "2021-01-01"),
            (3, "Second Code", "Hello Earth!", "2021-01-01"),
            (4, "Third Code", "Welcome to Mars!", "2021-01-01"),
            (6, "Fourth Code", "Bye bye solar system!", "2021-01-01");

-- get all the post with an author id of 3
-- na delete ko po kasi yung 1 and 2 nag try kasi ako sa date and time
SELECT * FROM posts WHERE user_id = 3;

-- get all the users email and datetime creation
SELECT email, datetime_created FROM users;

--update a post content to "Hello of the people of the Earth!" where its intial content is "Hello Earth!" by using the records id

UPDATE posts SET content = "Hello of the people of the Earth!" WHERE id = 2;

-- Delete the user with an email of "johndoe@gmail.com"

DELETE FROM users WHERE email = "johndoe@gmail.com";